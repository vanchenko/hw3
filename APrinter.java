package annotation;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Scanner;

public class APrinter {
    public void print(int a) {
        System.out.println(a);
    }
}

class Test {
    public static void main(String[] args) {
        Method[] declaredMethods = APrinter.class.getDeclaredMethods();
        Method print = Arrays.stream(declaredMethods).filter(method -> method.getName().equals("print")).findFirst().orElseThrow();
        print.setAccessible(true);
        Scanner scanner = new Scanner(System.in);

        APrinter aPrinter = new APrinter();
        while (true) {
            String s = scanner.nextLine();
            try {
                print.invoke(aPrinter, Integer.parseInt(s));
            } catch (IllegalAccessException e) {
                System.out.println("Передан неверный объект в метод при вызове метода с помощью рефлексии");
            }catch (InvocationTargetException e){
                System.out.println("Ошибка вызываемого метода");
            } catch (NumberFormatException e) {
                System.out.println("Введено не целое число");
            }
        }

    }
}

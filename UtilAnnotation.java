package annotation;

import java.lang.annotation.Annotation;

public class UtilAnnotation {
    public static boolean checkAnnotationIsLike(Class tClass) {
        Annotation[] annotations = tClass.getAnnotations();

        for (Annotation annotation : annotations) {
            System.out.println(annotation);
            if (annotation instanceof IsLike) {
                return true;
            }
        }
        return false;
    }
}

@IsLike(value = true)
class TestAnnotation {
    boolean test;
}

class Main {
    public static void main(String[] args) {
        System.out.println("Класс Test имеет аннотацию IsLike? - "
                + UtilAnnotation.checkAnnotationIsLike(TestAnnotation.class));
        System.out.println("Класс String имеет аннотацию IsLike? - "
                + UtilAnnotation.checkAnnotationIsLike(String.class));
    }
}

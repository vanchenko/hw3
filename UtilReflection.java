package annotation;

import java.util.*;

public class UtilReflection {
    public static <T> Set<String> getAllInterfaces(T object) {
        Set<String> set = new HashSet<>();
        Class<?> aClass = object.getClass();
        Class<?>[] interfaces = aClass.getInterfaces(); // интерфейсы собственные
        for (Class<?> interfaceElement : interfaces) {
            set.add(interfaceElement.getName());
        }

        List<Class<?>> interfacesParents = Arrays.asList(aClass.getSuperclass().getInterfaces());
        for (Class<?> interfaceElement : interfacesParents) {
            set.add(interfaceElement.getName());
        }

        for (Class<?> anInterface : interfaces) {
            Class<?>[] interfacesParentInterfaces = anInterface.getInterfaces();
            for (Class<?> interfaceElement : interfacesParentInterfaces) {
                set.add(interfaceElement.getName());
            }
        }


        return set;
    }


    public static void main(String[] args) {
        Set<String> allInterfaces = getAllInterfaces(new ArrayList<>());
        for (String name : allInterfaces) {
            System.out.println(name);
        }
    }
}


